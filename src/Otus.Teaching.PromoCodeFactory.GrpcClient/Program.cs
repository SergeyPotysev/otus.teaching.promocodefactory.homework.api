﻿using System;
using System.Threading.Tasks;
using Grpc.Net.Client;
using Otus.Teaching.PromoCodeFactory.GrpcService;

namespace gRPCClient
{
    class Program
    {
        static async Task Main(string[] args)
        {
            // Создаем канал для обмена сообщениями с сервером. Параметр - адрес сервера gRPC
            using var channel = GrpcChannel.ForAddress("https://localhost:5001");
            // Создаем клиента
            var client = new UserSrv.UserSrvClient(channel);
            
            // Обмениваемся сообщениями с сервером
            
            // Получаем список всех пользователей
            var customers = await client.GetAllCustomersAsync(new CustomerRequest() { Id = "*" });
            var i = 1;
            Console.WriteLine("Всего клиентов: " + customers.Customers.Count);
            foreach (var customer in customers.Customers)
            {
                Console.WriteLine($"{i++}. {customer.Id}: {customer.FirstName} {customer.LastName}");
            }
            
            // Получаем одного пользователя по Guid
            var customer1 = await client.GetCustomerByIdAsync(new CustomerRequest() { Id = "679decbe-8455-4d9b-a483-bc564cd42675" });
            Console.WriteLine($"Привет, {customer1.FirstName } {customer1.LastName}!");
            Console.ReadKey();
        }
    }
}
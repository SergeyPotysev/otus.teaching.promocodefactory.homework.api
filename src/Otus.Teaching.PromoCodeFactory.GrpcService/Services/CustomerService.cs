using System;
using System.Threading.Tasks;
using Grpc.Core;
using Microsoft.Extensions.Logging;
using Otus.Teaching.PromoCodeFactory.Core.Abstractions.Repositories;
using Otus.Teaching.PromoCodeFactory.Core.Domain.PromoCodeManagement;

namespace Otus.Teaching.PromoCodeFactory.GrpcService.Services
{
    public class CustomerService : UserSrv.UserSrvBase
    {
        private readonly ILogger<CustomerService> _logger;
        private readonly IRepository<Customer> _customerRepository;

        public CustomerService(ILogger<CustomerService> logger, IRepository<Customer> customerRepository)
        {
            _logger = logger;
            _customerRepository = customerRepository;
        }

        
        public override async Task<CustomersReply> GetAllCustomers(CustomerRequest request, ServerCallContext context)
        {
            var customers = await _customerRepository.GetAllAsync();
            
            var customersReply = new CustomersReply();
            foreach (var customer in customers)
            {
                customersReply.Customers.Add(new CustomerReply()
                {
                    Id = customer.Id.ToString(),
                    Email = customer.Email,
                    FirstName = customer.FirstName,
                    LastName = customer.LastName
                });
            }
            return customersReply;
        }
        
        
        public override Task<CustomerReply> GetCustomerById(CustomerRequest request, ServerCallContext context)
        {
            var customer =  _customerRepository.GetByIdAsync(Guid.Parse(request.Id));
            return Task.FromResult(new CustomerReply()
            {
                Id = request.Id,
                Email = customer.Result.Email,
                FirstName = customer.Result.FirstName,
                LastName = customer.Result.LastName
            });
        }
    }
}
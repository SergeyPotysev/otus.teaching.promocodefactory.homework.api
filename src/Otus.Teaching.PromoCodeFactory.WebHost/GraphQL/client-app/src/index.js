import React from 'react';
import { render } from 'react-dom';
import { ApolloProvider } from '@apollo/client/react';
import { ApolloClient, InMemoryCache } from '@apollo/client';
import { useQuery, gql } from '@apollo/client';

const client = new ApolloClient({
    uri: 'http://localhost:5000/graphql',
    fetchOptions: {
        mode: 'no-cors',
    },
    cache: new InMemoryCache()
});


const CUSTOMER = gql`
    query OneCustomer($id: Uuid!) {
        customer(id: $id) {
            email
            fullName
        }
    }`;


const CUSTOMERS = gql`
  query AllCustomers {
      customers {
        totalCount
        nodes {
          id
          email
          fullName
          preferences {
            preference {
              name
            }
          }
        }
      }
  }`;


function GetCustomers() {
    const { loading, error, data } = useQuery(CUSTOMERS);

    if (loading) return <p>Loading...</p>;
    if (error) return <p>Error :(</p>;

    return (
        <div>
            <p>Всего клиентов: {data.customers.totalCount}</p>
            <div>
                {data.customers.nodes.map(({ id, email, fullName }, i) => (
                <div key={id}>
                <p>
                    {i+1}. {id}: {email}, {fullName}
                </p>
                </div>
            ))}
            </div>
        </div>
    );
}


function GetCustomer() {
    const { loading, error, data } = useQuery(CUSTOMER, {
        variables: { id: '679decbe-8455-4d9b-a483-bc564cd42675' }
    });

    if (loading) return <p>Loading...</p>;
    if (error) return <p>Error :(</p>;

    return <p>Привет, {data.customer.fullName}!</p>;
}


function App() {
    return (
        <div>
            <h2>My first Apollo app 🚀</h2>
            <GetCustomers/>
            <GetCustomer/>
        </div>
    );
}


render(
    <ApolloProvider client={client}>
        <App />
    </ApolloProvider>,
    document.getElementById('root'),
);
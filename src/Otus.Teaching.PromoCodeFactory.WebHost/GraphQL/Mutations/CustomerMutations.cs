﻿using System;
using System.Threading.Tasks;
using HotChocolate;
using HotChocolate.Types;
using Microsoft.AspNetCore.Cors;
using Otus.Teaching.PromoCodeFactory.Core.Abstractions.Repositories;
using Otus.Teaching.PromoCodeFactory.Core.Domain.PromoCodeManagement;
using Otus.Teaching.PromoCodeFactory.WebHost.Mappers;
using Otus.Teaching.PromoCodeFactory.WebHost.Models;

namespace Otus.Teaching.PromoCodeFactory.WebHost.GraphQL.Mutations
{
    
    [ExtendObjectType(Name = "Mutation")]
    public class CustomerMutations
    {
        private readonly IRepository<Preference> _preferenceRepository;

        public CustomerMutations(IRepository<Preference> preferenceRepository)
        {
            _preferenceRepository = preferenceRepository;
        }
        
        /// <summary>
        /// Добавить нового клиента.
        /// </summary>
        [DisableCors]
        public async Task<CustomerResponse> CreateCustomer(
            CreateOrEditCustomerRequest request, 
            [Service]IRepository<Customer> repository)
        {
            var preferences = await _preferenceRepository.GetRangeByIdsAsync(request.PreferenceIds);
            var customer = CustomerMapper.MapFromModel(request, preferences);
            await repository.AddAsync(customer);
            return new CustomerResponse(customer);
        }
        
        
        /// <summary>
        /// Удалить клиента по Guid.
        /// </summary>
        [DisableCors]
        public async Task<CustomerShortResponse> DeleteCustomer(Guid id, [Service]IRepository<Customer> repository)
        {
            var customer = await repository.GetByIdAsync(id);
            await repository.DeleteAsync(customer);
            return new CustomerShortResponse()
            {
                Id = customer.Id,
                Email = customer.Email,
                FirstName = customer.FirstName,
                LastName = customer.LastName
            };
        }
    }
}
﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using HotChocolate;
using HotChocolate.Types;
using HotChocolate.Types.Relay;
using Microsoft.AspNetCore.Cors;
using Otus.Teaching.PromoCodeFactory.Core.Abstractions.Repositories;
using Otus.Teaching.PromoCodeFactory.Core.Domain.PromoCodeManagement;

namespace Otus.Teaching.PromoCodeFactory.WebHost.GraphQL.Queries
{
    [ExtendObjectType(Name = "Query")]
    public class CustomerQueries
    {
        /// <summary>
        /// Получить список клиентов.
        /// </summary>
        [UsePaging]
        [UseFiltering]
        [UseSorting]
        [DisableCors]
        public async Task<IEnumerable<Customer>> GetCustomers([Service]IRepository<Customer> repository) =>
            await repository.GetAllAsync();
        
        
        /// <summary>
        /// Получить одного клиента по Guid.
        /// </summary>
        //TODO -- С атрибутом [UsePaging] метод не работает! --
        [DisableCors]
        public async Task<Customer> GetCustomer(Guid id, [Service]IRepository<Customer> repository) =>
            await repository.GetByIdAsync(id);
    }
}